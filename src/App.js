import React from 'react';
import Slider from './components/Slider';
import Menu from './components/Menu';
import './styles/App.css'


function App() {
  return (
    <div className="App">
      <Menu/>
      <Slider/>
    </div>
  );
}

export default App;
