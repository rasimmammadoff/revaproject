import React from 'react';
import '../styles/Menu.css';
import icon from '../images/icon.png'



function Menu() {
  return (
    <div className="Menu">
      <button className="arrow">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
      </button>

      <img className="icon" src={icon} />
      
      <div className="nav">
        <ul>
          <li className="active"><a href=""><i class="fa fa-home" aria-hidden="true"></i></a></li>
          <li><a href=""><i class="fa fa-user-o" aria-hidden="true"></i></a></li>
          <li><a href=""><i class="fa fa-picture-o" aria-hidden="true"></i></a></li>
          <li><a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i></a></li>
          <li><a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
        </ul>
      </div>


    </div>
  );
}

export default Menu;
