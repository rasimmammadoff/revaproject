import React, { useState, useEffect } from 'react';
import img1 from '../images/slide1.jpg';
import img2 from '../images/slide2.jpg';
import img3 from '../images/slide3.jpg';
import '../styles/Slider.css';

function Slider() {

  let images = [img1, img2, img3]

  const [index, setIndex] = useState(0)
  const [show, setShow] = useState(false)

  useEffect(() => {
    setTimeout(() => setIndex((prev) => (prev + 1) % 3), 7000)
  })

  const changeImg = (where) => {
    if (where === 'back') {
      setIndex(prev => {
        let index = prev - 1
        if (index < 0) {
          index = index + images.length
          return index
        }
        return index % images.length
      })
    }
    else if (where === 'forward') {
      setIndex(prev => (prev + 1) % images.length)
    }
  }

  const openColors = () => {
    setShow(prev => !prev)
  }

  return (
    <div className="Slider">
      <div className="container" style={show ? { right: "0px" } : { right: "-210px" }}>
        <div onClick={openColors} className="gear">
          <i class="fa fa-cog" aria-hidden="true"></i>
        </div>
        <div className="colors">
          Colors
        </div>
      </div>

      <img className="images" src={images[index]} />
      
      <div className="buttons">
        <button onClick={() => changeImg('back')}><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
        <button onClick={() => changeImg('forward')}><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
      </div>
    </div>
  );
}

export default Slider;
